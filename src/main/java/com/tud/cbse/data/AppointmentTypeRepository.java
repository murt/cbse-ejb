/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tud.cbse.data;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import com.tud.cbse.model.AppointmentType;
import com.tud.cbse.model.Researcher;

@ApplicationScoped
public class AppointmentTypeRepository {

    @Inject
    private EntityManager em;

    public AppointmentType findById(Long id) {
        return em.find(AppointmentType.class, id);
    }

    public List<AppointmentType> findAllOrderedByTitle() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppointmentType> criteria = cb.createQuery(AppointmentType.class);
        Root<AppointmentType> appointmentType = criteria.from(AppointmentType.class);
        // Swap criteria statements if you would like to try out type-safe criteria queries, a new
        // feature in JPA 2.0
        // criteria.select(member).orderBy(cb.asc(member.get(Member_.name)));
        criteria.select(appointmentType).orderBy(cb.asc(appointmentType.get("title")));
        return em.createQuery(criteria).getResultList();
    }
}
